# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
**在html部分 我簡單分為三個部分**
* 其一是我的功能鍵
    拉桿用了range type的功能
    選單則用了select與option 
* 其二是canvas
* 其三是調整顏色（又分為直接選取與內建的color type）

**CSS**
* 大部分為美工排版
* 當中我用了一個hover，當滑鼠靠碰到按鈕時，按鈕會做指定的變化(選顏色部分)

**JS**
* window.onload 
    * 在開啟頁面時先跑function內的東西，可使網頁較流暢
    * 一開始即建立一個2d的畫布（預設為透明,而我將畫布塗成白色）
* getMousePos
    * 我紀錄的當下滑鼠的x,y值
* MouseMove
    * 會根據目前不同的mode，來執行不同的繪圖。
    * mode 0 = pen, mode 1 = eraser, mode 2 = rectangle
    * mode 3 = circle, mode 4 = text, mode 5 = triangle
* loadImage
    * 讓我畫圖型時能夠隨時根據滑鼠的移動繪出不同大小(ex長方形會跟著滑鼠變大變小)
    * 我在每次移動會刷新整個canvas，並載入上個時段紀錄的canvas
* record
    * 紀錄canvas的圖，利用**toDateURL**這個function
    * 將每次的canvas得值記錄下來，在存到array中，以利undo redo運作
* mousedown、mousemove、mouseup 
    * 對於canvas上的滑鼠動作，我有3個**addEventListener**，根據mousedown、mousemove、mouseup等動作做不同功能。
    * 在mousedown中，我針對text特別動作，因為按下去要能立即打字，而其他均在mousemove中動作。
    * 繪圖部分，先呼叫**beginPath**，並用**moveTo**、**lineTo**跟**stroke**來繪圖
* 在canvas上方button(各種功能)
    * 根據滑鼠點擊，來進行不同的設定，並更改mode以在mousemove中運作
* upload
    * 是利用**FileReader**將檔案讀取，並將圖片印到canvas上面
* download
    * 是利用< a >這個方式，將目前canvas的**url**下載（預設名稱為test.jpg）
* reset
    * 是將整個頁面清掉，並將功能定義為筆刷
* 下方其他function則是對於筆刷大小寬度顏色等等的設定
* text
    * 先create一個input的Element，並將type設為text
    * 利用**handleEnter**取得鍵盤的enter，在傳到**drawText**將文字畫在canvas上

**基本功能**
**Basic control tools**
1. Brush and eraser
    * Color selector -> under the canvas
    * Simple menu (brush size) -> use pull rod the change size
2. Text input
    * User can type texts on canvas – Font menu (typeface and size)
    * User can change text type and size by menu 
3. Cursor icon
    * The image should change according to the currently used tool
    * brush has a pen icon and eraser has an eraser icon
    * when using shape we have crosshir cursor
4. Refresh button – Reset canvas

**Advance tools**
1. Different brush shapes
    * Circle, rectangle and triangle
2. Un/Re-do button
3. Image tool - Upload and Download


**特殊功能**
* 在更換顏色後，切換筆刷、圖形等工具時能保留顏色更改
* 在下方選擇顏色時，滑鼠靠近會有不同動作
* 在拉動圖形時，我的圖形會根據滑鼠的位置隨時改變，此方法式利用當滑鼠移動時，刷新整個canvas，並載入上個時段的canvas


