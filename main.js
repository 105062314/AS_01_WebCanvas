var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");

var x = 0;
var y = 0;
var a = "";
var mode = 0;
var begin_x = 0;
var begin_y = 0;
var url = "";
var url_index = 0;
var max_index = 0;
var history = new Array(100);
var font = '14px sans-serif';
var hasInput = false;
var px = 45; //校正筆刷、橡皮擦圖案誤差

window.onload = function(){
    ctx.strokeStyle = "#000000";
    ctx.lineWidth = 6;
    ctx.fillStyle="#FFFFFF";
    ctx.fillRect(0,0,800,500);
    canvas.style.cursor = "url(01.png) ,auto";
    url_index = -1;
    max_index = 0;
    record();
};
function getMousePos(canvas, e){
    var info = canvas.getBoundingClientRect();
    return{
        //x: e.pageX - info.left,
        //y: e.pageY - info.top
        x: e.offsetX ,
        y: e.offsetY+px
    };
}
function MouseMove(e){
    var mousepos = getMousePos(canvas,e);
    if(mode==0){
        ctx.lineTo(mousepos.x,mousepos.y);
        ctx.stroke();
    }
    else if(mode==1){
        ctx.lineTo(mousepos.x,mousepos.y);
        ctx.stroke();
    }
    else if(mode==2){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.fillStyle="#FFFFFF";
        ctx.fillRect(0,0,800,500);
        loadImage();
        ctx.strokeRect(begin_x,begin_y,mousepos.x-begin_x,mousepos.y-begin_y);
    }
    else if(mode==3){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.fillStyle="#FFFFFF";
        ctx.fillRect(0,0,800,500);
        loadImage();
        ctx.beginPath();
        var rx = (mousepos.x-begin_x)/2;
        var ry = (mousepos.y-begin_y)/2;
        var r = Math.sqrt(rx*rx+ry*ry);
        ctx.arc(rx+begin_x,ry+begin_y,r,0,Math.PI*2);
        ctx.stroke();
    }
    else if(mode==5){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.fillStyle="#FFFFFF";
        ctx.fillRect(0,0,800,500);
        loadImage();
        ctx.beginPath();
        ctx.moveTo(begin_x,begin_y);
        ctx.lineTo(mousepos.x-100,begin_y);
        ctx.lineTo(mousepos.x,mousepos.y);
        ctx.closePath();
        ctx.stroke();
    }
}
function loadImage(){
    var img = new Image();
    img.src = url;
    ctx.drawImage(img,0,0,canvas.width,canvas.height);
}
function record(){
    url_index += 1;
    if(url_index>max_index)
        max_index = url_index;
    history[url_index] = canvas.toDataURL();
}
function Undo() {
    if (url_index > 0) {
        url_index--;
        var canvasPic = new Image();
        canvasPic.src = history[url_index];
        //url = history[url_index];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0,canvas.width,canvas.height); }
    }
}
function Redo() {
    if (url_index < max_index) {
        url_index+=1;
        var canvasPic = new Image();
        canvasPic.src = history[url_index];
        //url = history[url_index];
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0,canvas.width,canvas.height); }
    }
}
canvas.addEventListener('mousedown',function(e){ //偵測按下滑鼠
    var mousepos = getMousePos(canvas,e);
    begin_x = mousepos.x;
    begin_y = mousepos.y;

    if(mode==4){    //text
        if(!hasInput)
            addInput(e.clientX, e.clientY);
    }
    else{
        ctx.beginPath();
        ctx.moveTo(mousepos.x,mousepos.y);
        canvas.addEventListener('mousemove',MouseMove);
    }
    //偵測移動的滑鼠
});
canvas.addEventListener('mouseup', function() {
    canvas.removeEventListener('mousemove', MouseMove);
    url = canvas.toDataURL();
    record();
    //console.log(history[url_index]);
});
document.getElementById("pen").addEventListener("click",function() {
    if(mode==1)
        ctx.strokeStyle = "black";
    else
        ctx.strokeStyle = a;
    canvas.style.cursor = "url(01.png) ,auto";
    px = 45;
    mode = 0;
});
document.getElementById("eraser").addEventListener("click",function() {
    mode = 1;
    ctx.strokeStyle = "white";
    canvas.style.cursor = "url(02.png) ,auto";
    px = 45;
});
document.getElementById("rect").addEventListener("click",function() {
    if(mode==1)
        ctx.strokeStyle = "black";
    else
        ctx.strokeStyle = a;
    mode = 2;
    px = 0;
    canvas.style.cursor = "crosshair";
});
document.getElementById("circle").addEventListener("click",function() {
    if(mode==1)
        ctx.strokeStyle = "black";
    else
        ctx.strokeStyle = a;
    mode = 3;
    px = 0;
    canvas.style.cursor = "crosshair";
});
document.getElementById("text").addEventListener("click",function() {
    mode = 4;
    px = 0;
    canvas.style.cursor = "text";
});
document.getElementById("tri").addEventListener("click",function() {
    if(mode==1)
        ctx.strokeStyle = "black";
    else
        ctx.strokeStyle = a;
    mode = 5;
    px = 0;
    canvas.style.cursor = "crosshair";
});
document.getElementById("undo").addEventListener("click",function() {
    document.body.style.cursor = "w-resize";
    Undo();
});
document.getElementById("redo").addEventListener("click",function() {
    document.body.style.cursor = "e-resize";
    Redo();
});

document.getElementById("download").addEventListener("click",function() {
    document.body.style.cursor = "default";
    this.href = document.getElementById("canvas").toDataURL();
    this.download = "test.jpg";
});

var imageloader = document.getElementById("upload");
imageloader.addEventListener("change",function(e) {
    var reader  = new FileReader();
    document.body.style.cursor = "default";
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
            record();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    record();
});

document.getElementById("reset").addEventListener("click",
    function() {
        canvas.width = 800;
        canvas.height = 500;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        url_index = -1;
        ctx.fillStyle="white";
        ctx.fillRect(0,0,800,500);
        url = canvas.toDataURL();
        canvas.style.cursor = "url(01.png) ,auto";
        ctx.lineWidth = 6;
        mode = 0;
        px = 45;
        record();
},false);   

document.getElementById("colorboard").addEventListener("change",function(){
    var colorboard = document.getElementById("colorboard").value;
    ctx.strokeStyle = colorboard;
});
function changeColor(a){
    if(mode!=1)
        ctx.strokeStyle = a;
}
var fontSize = 20;
var fontStyle;
function TextSize(size){ 
    fontSize = size;
}
function TextStyle(style){ 
    fontStyle = style;
}
document.getElementById("width").addEventListener("change",function(){
    var width = document.getElementById("width").value;
    ctx.lineWidth = width*2;
});

//text
function addInput(x, y) {
    
    var input = document.createElement('input');
    
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';

    input.onkeydown = handleEnter;
    
    document.body.appendChild(input);

    input.focus();
    
    hasInput = true;
}
function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}
function drawText(txt, x, y) {
    ctx.textBaseline = 'top';
    ctx.textAlign = 'left';

    ctx.font = fontSize+"px "+fontStyle;
    ctx.fillStyle = "black";
    record();
    ctx.fillText(txt, x - 240, y - 100);
}